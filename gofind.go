package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var (
	gopath = os.Getenv("GOPATH") + "/src"

	target string
	match  string
	found  = fmt.Errorf("Found match")

	tovisit []string
	visited = make(map[string]struct{})
)

func main() {

	if len(os.Args) > 1 {
		target = os.Args[1]

		paths := strings.Split(gopath, ";")
		tovisit = append(tovisit, paths...)
		if err := visit(walkfunc); err != nil {
			log.Println(err)
			match = ""
		}
	}

	if target == "" {
		fmt.Printf("%s\n", gopath)
	} else if match == "" {
		fmt.Fprintf(os.Stderr, "No matching package found for %q\n", target)
		pwd, err := os.Getwd()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%s\n", pwd)
		os.Exit(1)
	}
	fmt.Printf("%s\n", match)
}

func visit(walkfunc filepath.WalkFunc) error {

	for len(tovisit) > 0 {

		// Get the next directory to explore
		next := tovisit[0]
		tovisit = tovisit[1:]

		// Find the actual target of the path
		absPath, err := abs(next)
		if err != nil {
			return err
		}

		// Check if we've visited this location before
		if _, ok := visited[absPath]; ok {
			continue
		}
		visited[absPath] = struct{}{}

		// Check this is a directory
		info, err := os.Stat(next)
		if err != nil {
			if os.IsNotExist(err) {
				return nil
			}
			return err
		}

		if info.IsDir() {
			// Process dir
			err = walkfunc(next, info, err)
			if err == filepath.SkipDir {
				continue
			} else if err == found {
				break
			} else if err != nil {
				return err
			}

			// Add to queue
			if err := appendSubDirs(next); err != nil {
				return err
			}
		}
	}

	return nil
}

func abs(next string) (absPath string, err error) {
	linkPath, err := os.Readlink(next)
	if err != nil {
		absPath, err = filepath.Abs(next)
		if err != nil {
			return next, err
		}
	} else {
		if !filepath.IsAbs(linkPath) {
			linkPath = filepath.Join(next, "..", linkPath)
		}
		absPath, err = filepath.Abs(linkPath)
		if err != nil {
			return next, err
		}
	}

	return
}

func appendSubDirs(next string) error {
	f, err := os.Open(next)
	if err != nil {
		return err
	}
	defer f.Close()

	names, err := f.Readdirnames(0)
	if err != nil {
		return err
	}

	for _, name := range names {
		tovisit = append(tovisit, filepath.Join(next, name))
	}

	return nil
}

func walkfunc(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	if _, file := filepath.Split(path); strings.HasPrefix(file, ".") {
		return filepath.SkipDir
	}

	if strings.HasSuffix(path, "/"+target) {
		match = path
		return found
	}

	return nil
}
