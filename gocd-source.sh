#!/bin/bash

## Source this file or add it to your .profile to add support for the gocd command
function gocd {
    cd `gofind $1`
}  
