# README #
To install the `gofind` tool, run the following command:

~~~ sh
go install bitbucket.org/mikehouston/gofind
~~~

To use the tool for quickly changing directory to a Go source folder, copy the contents of `gocd-source.sh` to your `.profile` file. This will add a function
to your shell which allows you to use, for example:

~~~ sh
gocd gofind
~~~

This will move you to the most shallowly-nested package of that name. The search works on suffixes, so you can also be more specific:

~~~
gocd mikehouston/gofind
~~~

### Contribution guidelines ###

I'm happy to accept fixes or improvements via pull request. I'm not planning any new features however.

### License

This code is licensed under the MIT license.